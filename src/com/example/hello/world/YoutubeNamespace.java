package com.example.hello.world;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

public class YoutubeNamespace implements NamespaceContext {

    public String getNamespaceURI(String prefix) {
        if (prefix == null) throw new NullPointerException("Null prefix");
        else if ("default".equals(prefix)) return "http://www.w3.org/2005/Atom";
        else if ("media".equals(prefix)) return "http://search.yahoo.com/mrss/";
        else if ("gd".equals(prefix)) return "http://schemas.google.com/g/2005";
        else if ("yt".equals(prefix)) return "http://gdata.youtube.com/schemas/2007";
        return XMLConstants.NULL_NS_URI;
    }

    // This method isn't necessary for XPath processing.
    public String getPrefix(String uri) {
        throw new UnsupportedOperationException();
    }

    // This method isn't necessary for XPath processing either.
    public Iterator getPrefixes(String uri) {
        throw new UnsupportedOperationException();
    }

}