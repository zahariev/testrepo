package com.example.hello.world;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;

public class MainActivity extends Activity{
	public Button goButton;
	public Button youtubeButton;
	public Gallery picGallery;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.goButton = (Button)findViewById(R.id.button1);
        this.goButton.setOnClickListener(new ButtonClickEvent(this));
        this.youtubeButton = (Button)findViewById(R.id.youtube);
        this.youtubeButton.setOnClickListener(new YoutubeClickEvent(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true; 
    }
    
}
