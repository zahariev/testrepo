package com.example.hello.world;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;

import android.widget.TextView;

public class GetRequest {

	public static InputStream openGetRequestStream(String u){
		try {
			URL url = new URL(u);
			URLConnection urlConnection;
			if (url.getProtocol()=="https"){
		        HttpsURLConnection tmp = (HttpsURLConnection) url.openConnection();
		        tmp.setHostnameVerifier(new CheatVerifier());
		        urlConnection = tmp;
			}
			else
				urlConnection = (HttpURLConnection) url.openConnection();
	        return (InputStream)urlConnection.getContent();
		}
		catch (Exception e){
			String test = e.toString();
			test = "";
			return null;
		}
	}
	
	
}
