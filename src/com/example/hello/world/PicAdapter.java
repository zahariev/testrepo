package com.example.hello.world;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class PicAdapter extends BaseAdapter {
	private Context galleryContext;
	private Bitmap[] imageBitmaps;
	
	public PicAdapter(Context c, ArrayList<String> urls) {
	    galleryContext = c;
	    imageBitmaps  = new Bitmap[urls.size()];
	    int i = 0;
	    for (String url: urls){
	    	imageBitmaps[i] = BitmapFactory.decodeStream(GetRequest.openGetRequestStream(url));
	    	i++;
	    }
	}
	
	public int getCount() {
	    return imageBitmaps.length;
	}
	
	public Object getItem(int position) {
	    return position;
	}
	
	public long getItemId(int position) {
	    return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
	    //create the view
	    ImageView imageView = new ImageView(galleryContext);
	    imageView.setImageBitmap(imageBitmaps[position]);
	    imageView.setLayoutParams(new Gallery.LayoutParams(160, 90));
	    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
	    return imageView;
	}
	
	
}
