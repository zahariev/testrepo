package com.example.hello.world;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class ButtonClickEvent implements OnClickListener {
	private Activity activity;
	public ButtonClickEvent(Activity act){
		this.activity = act;
	}
	
	public void onClick(View arg0) {
		EditText text = (EditText)activity.findViewById(R.id.editText1);
		String name = text.getText().toString();
		if (name.length() == 0) {
		    new AlertDialog.Builder(activity) 
		            .setMessage("You must enter your name!")
		            .setNeutralButton("Ok", null)
		            .show();
		    return;
		}
		else {
			new AlertDialog.Builder(activity) 
	            .setMessage("Hello "+name+"!")
	            .setNeutralButton("Ok", null)
	            .show();
			return;
		}
		
	}
}
