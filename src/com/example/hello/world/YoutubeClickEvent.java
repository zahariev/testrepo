package com.example.hello.world;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.TextView;

public class YoutubeClickEvent implements OnClickListener {
	private MainActivity activity;
	private TextView view;
	
	public YoutubeClickEvent(MainActivity act){
		this.activity = act;
	}

	public void onClick(View arg0) {
		EditText text = (EditText)activity.findViewById(R.id.editText1);
		String name = text.getText().toString();
		if (name.length() == 0) {
		    new AlertDialog.Builder(activity) 
		            .setMessage("You must enter a Youtube Video ID!")
		            .setNeutralButton("Ok", null)
		            .show();
		    return;
		}
		else {
			name = "bQrTU3oCwI0";
			String URL = "https://gdata.youtube.com/feeds/api/videos/"+name+"?v=2";
			this.view = (TextView)activity.findViewById(R.id.textView1);
			String XML = sendGetRequest(URL);
			if (XML != ""){
				Document doc = parseXML(XML);
				if (doc != null){
					/*String title = getTitle(doc);
					this.view.setText(title);*/
					this.loadThumbnails(this.getThumbnails(doc));
				}
			}
		}
	}
	
	private void loadThumbnails(ArrayList<String> thumbs){
		activity.picGallery = (Gallery) activity.findViewById(R.id.gallery);
		PicAdapter imgAdapt = new PicAdapter((Context)activity, thumbs);
		activity.picGallery.setAdapter(imgAdapt);
	}
	
	private String sendGetRequest(String url){
		try{
			InputStream stream = GetRequest.openGetRequestStream(url);
			StringWriter writer = new StringWriter();
            
            char[] buffer = new char[1024];
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(stream, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
                stream.close();
                return writer.toString();
		}
		catch (IOException e){
			handleException(e);
			return "";
		}
	}
	
	private Document parseXML(String XML){
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(XML)));
			return doc;
		}
		catch (Exception e){
			handleException(e);
			return null;
		}
	}
	
	private String getTitle(Document doc){
		try{
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			xpath.setNamespaceContext(new YoutubeNamespace());
			XPathExpression expr = xpath.compile("//default:title/text()");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			//return ""+nodes.getLength();
			Node node = nodes.item(0);
			return node.getNodeValue().toString();
		}
		catch (Exception e){
			handleException(e);
			return e.toString();
		}
	}
	
	private ArrayList<String> getThumbnails(Document doc){
		ArrayList<String> output = new ArrayList<String>();
		NodeList thumbnails = (NodeList)doc.getElementsByTagName("media:thumbnail");
		for (int i=0; i<thumbnails.getLength();i++){
			Element elem = (Element) thumbnails.item(i);
			output.add(elem.getAttribute("url"));
		}
        return output;
	}
	
	private void handleException(Exception e){
		this.view.setText(e.toString());
	}
	
	private void handleException(Exception e, String append){
		this.view.setText(e.toString()+" "+append);
	}
}
